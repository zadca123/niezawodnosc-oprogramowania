#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int resultModified;
    int result;
    int statementsCount;
    int firstLine;
    int linesCount;
    char fileName[256];
    char functionName[256];
    char originalResult[512];
} McCabeFunctionResult;


#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

FILE *call_pmccabe(char* filename);
int count_output_lines(char* filename);
int parse_pmccabe_output(FILE *pmccabe_output, McCabeFunctionResult *result);
void print_result(McCabeFunctionResult *result, int resultSize, char* filename);
void print_single_result(McCabeFunctionResult singleResult);

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Użycie: %s <plik-źródłowy>\n", argv[0]);
        return 1;
    }

    FILE * pmccabe_output = call_pmccabe(argv[1]);
    if (pmccabe_output == NULL) {
        perror("Błąd: Próba uruchomienia polecenia pmccabe zakończyła się niepowodzeniem\n");
        return 2;
    }

    int output_lines_count = count_output_lines(argv[1]);
    McCabeFunctionResult *result = malloc(output_lines_count * sizeof(McCabeFunctionResult));

    int parse_result = parse_pmccabe_output(pmccabe_output, result);
    pclose(pmccabe_output);
    if(parse_result != 0) {
        free(result);
        return 3;
    }

    print_result(result, output_lines_count, argv[1]);

    free(result);
    return 0;
}

FILE *call_pmccabe(char* filename) {
    char command[256];
    snprintf(command, sizeof(command), "pmccabe %s", filename);
    return popen(command, "r");
}

int count_output_lines(char* filename) {
    FILE * pmccabe_output = call_pmccabe(filename);
    int lineCount = 0;
    char buffer[1024];
    while (fgets(buffer, sizeof(buffer), pmccabe_output) != NULL) {
        lineCount++;
    }
    pclose(pmccabe_output);
    return lineCount;
}

int parse_pmccabe_output(FILE * pmccabe_output, McCabeFunctionResult *result) {
    char buffer[1024];
    int index = 0;
    while(fgets(buffer, sizeof(buffer), pmccabe_output) != NULL) {
        char functionName[256];
        int readVariables = sscanf(buffer, "%d %d %d %d %d %s %s\n",
                &result[index].resultModified,
                &result[index].result,
                &result[index].statementsCount,
                &result[index].firstLine,
                &result[index].linesCount,
                result[index].fileName,
                result[index].functionName
        );
        strncpy(result[index].originalResult, buffer, sizeof(result[index].originalResult) - 1);
        index++;
        if(readVariables != 7) {
            perror("Błąd: nie udało się sparsować wartości zwróconej przez polecenie pmccabe\n");
            return 1;
        }
    }
    return 0;
}

void print_result(McCabeFunctionResult *result, int resultSize, char* filename) {
    int totalCNumber = 0;
    printf("Liczba cyklometryczna McCabe'a dla pliku " ANSI_COLOR_CYAN "%s" ANSI_COLOR_RESET ":\n", filename);
    if(resultSize == 1) {
        totalCNumber += result[0].result;
        printf("Analizowany plik składa się z " ANSI_COLOR_CYAN "1" ANSI_COLOR_RESET " funkcji o nazwie %s.\n", result[0].functionName);
        print_single_result(result[0]);
    } else {
        printf("Analizowany plik składa się z " ANSI_COLOR_CYAN "%d" ANSI_COLOR_RESET " funkcji.\n", resultSize);
        for(int i = 0; i < resultSize; i++) {
            totalCNumber += result[i].result;
            print_single_result(result[i]);
        }
    }
    totalCNumber -= (resultSize-1);
    printf("Liczba cyklometryczna McCabe'a dla całego programu wynosi " ANSI_COLOR_CYAN "%d" ANSI_COLOR_RESET ". Interpretacja: ", totalCNumber);
    if(totalCNumber < 11) {
        printf(ANSI_COLOR_GREEN "prosty moduł" ANSI_COLOR_RESET);
    } else if(totalCNumber < 21) {
        printf(ANSI_COLOR_YELLOW "złożony moduł" ANSI_COLOR_RESET);
    } else if(totalCNumber < 51) {
        printf(ANSI_COLOR_MAGENTA "bardzo złożony moduł" ANSI_COLOR_RESET);
    } else {
        printf(ANSI_COLOR_RED "testowanie niemożliwe" ANSI_COLOR_RESET);
    }
    printf(".\n");
}

void print_single_result(McCabeFunctionResult singleResult) {
    printf("* Liczba cyklometryczna McCabe'a dla funkcji " ANSI_COLOR_CYAN "%s" ANSI_COLOR_RESET " wynosi " ANSI_COLOR_CYAN "%d" ANSI_COLOR_RESET "\n\t%s", 
            singleResult.functionName, singleResult.result, singleResult.originalResult
    );
}
