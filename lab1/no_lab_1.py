# -*- coding: utf-8 -*-
"""no-lab1.ipynb

Automatically generated by Colab.

Original file is located at
    https://colab.research.google.com/drive/1qApfrh1BwFbWYw88TQd0JXOOr1KY1n3n
"""

from scipy.optimize import fsolve
import math

x0 = 30
a, b = 1043.96, 17216.77

def equation(N):
  try:
    result = 0
    for i in range(30):
      result += (a*N-b)/(N-i)
    result -= 30*a
    return result
  except ZeroDivisionError:
    return math.inf

def solve_scipy():
  return fsolve(equation, x0)

solve_scipy()[0]

def solve_dummy():
  min_val, min_arg = math.inf, None
  for i in range(30, 1000):
    result = abs(equation(i))
    if result < min_val:
      min_val = result
      min_arg = i
  return min_arg

solve_dummy()

def derivative(N):
  result = 0
  for i in range(30):
    result += (b-i*a)/pow(N-i,2)
  return result

def solve_newton_recurrent(x, k):
  y = equation(x)
  if abs(y) < 1e-5 or k > 100:
    return x
  return solve_newton_recurrent(x-y/derivative(x), k+1)

def solve_newton():
  return solve_newton_recurrent(x0, 1)

solve_newton()

