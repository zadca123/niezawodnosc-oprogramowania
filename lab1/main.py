import numpy as np
from scipy.optimize import minimize

# Dane z tabeli
czasy_awarii = [
    14.75,
    43.99,
    9.89,
    0.07,
    5.70,
    7.89,
    28.79,
    170.15,
    26.83,
    36.15,
    7.99,
    28.09,
    11.80,
    1.78,
    12.50,
    73.08,
    42.60,
    9.18,
    49.43,
    9.19,
    64.25,
    40.90,
    3.07,
    0.75,
    13.36,
    23.02,
    143.31,
    55.46,
    75.57,
    34.31,
]


# Funkcja log-likelihood dla rozkładu wykładniczego
def log_likelihood(params, data):
    N, phi = params
    lambda_ = 1 / phi
    log_likelihood = -N * np.log(lambda_) - lambda_ * sum(data)
    return -log_likelihood


# Inicjalizacja parametrów dla optymalizacji
initial_params = [1, 1]

# Optymalizacja metodą największej wiarygodności
result = minimize(
    log_likelihood, initial_params, args=(czasy_awarii,), bounds=[(0, None), (0, None)]
)

# Wyświetlenie wyników
N_est, phi_est = result.x
lambda_est = 1 / phi_est
k_est = N_est - len(czasy_awarii)
print(f"Oszacowana początkowa liczba błędów N: {N_est}")
print(f"Oszacowana intensywność awarii lambda: {lambda_est}")
print(f"Liczba pozostałych błędów do usunięcia k po 30. awarii: {k_est}")
