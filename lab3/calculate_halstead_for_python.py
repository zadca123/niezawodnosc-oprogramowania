#!/usr/bin/env python

import re
import math
import sys
from collections import Counter


def calculate_halstead_metrics(file_path):
    with open(file_path, "r") as file:
        code = file.read()

    # Słowa kluczowe Pythona
    keywords_python = [
        "False",
        "None",
        "True",
        "and",
        "as",
        "assert",
        "async",
        "await",
        "break",
        "class",
        "continue",
        "def",
        "del",
        "elif",
        "else",
        "except",
        "finally",
        "for",
        "from",
        "global",
        "if",
        "import",
        "in",
        "is",
        "lambda",
        "nonlocal",
        "not",
        "or",
        "pass",
        "raise",
        "return",
        "try",
        "while",
        "with",
        "yield",
    ]
    keywords_piped = "|".join(keywords_python)
    # Operatory Pythona
    # operators_regex = (
    # r"[+\-*/%&|^!=<>]=?|~|:|,|;|[()\[\]{}]|\b(?:and|or|not|is|in|lambda|yield)\b"
    # )
    operators_regex = (
        f"[+\\-*/%&|^!=<>]{{1,2}}|[~?:,;\\{{\\}}\\[\\]()]|(?:{keywords_piped})\\b"
    )
    # Znajdź wszystkie operatory, w tym z listy keywordów
    operators = re.findall(operators_regex, code)

    # Znajdź wszystkie ciągi znaków
    strings = re.findall(r'"(?:\\.|[^"\\])*"|\'(?:\\.|[^\'\\])*\'', code)
    string_counts = Counter(strings)

    # Usuń ciągi znaków z kodu
    code = re.sub(r'"(?:\\.|[^"\\])*"|\'(?:\\.|[^\'\\])*\'', "", code)

    # Znajdź wszystkie operandy, wykluczając słowa kluczowe
    operands = re.findall(r"\b\w+\b", code)
    operands = [word for word in operands if word not in keywords_python]

    # Zlicz wystąpienia operatorów i operandów
    operator_counts = Counter(operators)
    operand_counts = Counter(operands)

    # Dodaj ciągi znaków do operandów
    operand_counts.update(string_counts)

    # Obliczanie Halstead Metrics
    n1 = len(operator_counts)
    n2 = len(operand_counts)
    L1 = sum(operator_counts.values())
    L2 = sum(operand_counts.values())

    l = n1 + n2
    L = L1 + L2
    V = L * math.log2(l) if l > 0 else 0
    T = (n1 * L2) / (2 * n2) if n2 > 0 else 0
    E = V * T
    N = [V / 3000, (E ** (2 / 3)) / (3000)]

    print("Halstead Metrics:")
    return {
        "liczba różnych operatorów (n1)": n1,
        "liczba różnych operandów (n2)": n2,
        "łączna liczba operatorów (L1)": L1,
        "łączna liczba operandów (L2)": L2,
        "słownik programu (l)": l,
        "długość programu (L)": L,
        "objętość programu (V)": V,
        "trudność programu (T)": T,
        "wymagany nakład pracy (E)": E,
        "przewidywana liczba błędów (N)": N,
        "operator_counts": operator_counts,
        "operand_counts": operand_counts,
    }


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python halstead_metrics.py <file_path>")
        sys.exit(1)

    file_path = sys.argv[1]
    metrics = calculate_halstead_metrics(file_path)
    for key, value in metrics.items():
        if key in ["operator_counts", "operand_counts"]:
            print(f"+ {key}:")
            for item, count in value.items():
                print(f"  - {item} :: {count}")
        else:
            print(f"{key} :: {value}")
