#!/usr/bin/env python

import re
import math
import sys
from collections import Counter


def calculate_halstead_metrics(file_path):
    with open(file_path, "r") as file:
        code = file.read()

    keywords_c = [
        "auto",
        "break",
        "case",
        "char",
        "const",
        "continue",
        "default",
        "do",
        "double",
        "else",
        "enum",
        "extern",
        "float",
        "for",
        "goto",
        "if",
        "inline",
        "int",
        "long",
        "register",
        "restrict",
        "return",
        "short",
        "signed",
        "sizeof",
        "static",
        "struct",
        "switch",
        "typedef",
        "union",
        "unsigned",
        "void",
        "volatile",
        "while",
    ]
    keywords_piped = "|".join(keywords_c)
    regex = f"[+\\-*/%&|^!=<>]{{1,2}}|[~?:,;\\{{\\}}\\[\\]()]|(?:{keywords_piped})\\b"

    # Znajdź wszystkie operatory, w tym z listy keywordów
    operators = re.findall(regex, code)

    # Znajdź wszystkie ciągi znaków
    strings = re.findall(r'"(?:\\.|[^"\\])*"|\'(?:\\.|[^\'\\])*\'', code)
    string_counts = Counter(strings)

    # Usuń ciągi znaków z kodu
    code = re.sub(r'"(?:\\.|[^"\\])*"|\'(?:\\.|[^\'\\])*\'', "", code)

    # Znajdź wszystkie operandy, wykluczając słowa kluczowe
    operands = re.findall(r"\b\w+\b", code)
    operands = [word for word in operands if word not in keywords_c]

    # Zlicz wystąpienia operatorów i operandów
    operator_counts = Counter(operators)
    operand_counts = Counter(operands)

    # Dodaj ciągi znaków do operandów
    operand_counts.update(string_counts)

    # Obliczanie Halstead Metrics
    n1 = len(operator_counts)
    n2 = len(operand_counts)
    L1 = sum(operator_counts.values())
    L2 = sum(operand_counts.values())

    l = n1 + n2
    L = L1 + L2
    V = L * math.log2(l)
    T = (n1 * L2) / (2 * n2)
    E = V * T
    N = [V / 3000, (E ** (2 / 3)) / (3000)]

    print("Halstead Metrics:")
    return {
        "liczba różnych operatorów (n1)": n1,
        "liczba różnych operandów (n2)": n2,
        "łączna liczba operatorów (L1)": L1,
        "łączna liczba operandów (L2)": L2,
        "słownik programu (l)": l,
        "długość programu (L)": L,
        "objętość programu (V)": V,
        "trudność programu (T)": T,
        "wymagany nakład pracy (E)": E,
        "przewidywana liczba błędów (N)": N,
        "operator_counts": operator_counts,
        "operand_counts": operand_counts,
    }


metrics = calculate_halstead_metrics(sys.argv[1])
for key, value in metrics.items():
    if key in ["operator_counts", "operand_counts"]:
        print(f"+ {key}:")
        for item, count in value.items():
            print(f"  - {item} :: {count}")
    else:
        print(f"{key} :: {value}")
