#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>

#define MAX_TOKENS 1000

typedef struct {
  char *token;
  int count;
} Token;

Token operatory[MAX_TOKENS];
Token operandy[MAX_TOKENS];
int liczbaOperatorow = 0;
int liczbaOperandow = 0;

void dodajToken(Token tokens[], int *count, const char *token) {
  for (int i = 0; i < *count; i++) {
    if (strcmp(tokens[i].token, token) == 0) {
      tokens[i].count++;
      return;
    }
  }
  tokens[*count].token = strdup(token);
  tokens[*count].count = 1;
  (*count)++;
}

int czyOperator(const char *token) {
  char *operatory[] = {
    "+", "-", "*", "/", "%", "=", "==", "!=", "<", ">", "<=", ">=",
    "&&", "||", "!", "&", "|", "^", "<<", ">>", "++", "--", "+=", "-=",
    "*=", "/=", "%=", "&=", "|=", "^=", "<<=", ">>=",
    "if", "else", "for","return", "(", ")", "[", "]", "{","}", ";", ","
  };
  int liczbaOperatorow = sizeof(operatory) / sizeof(operatory[0]);
  for (int i = 0; i < liczbaOperatorow; i++) {
    if (strcmp(token, operatory[i]) == 0) {
      return 1;
    }
  }
  return 0;
}

void przetworzToken(const char *token) {
  if (czyOperator(token)) {
    dodajToken(operatory, &liczbaOperatorow, token);
  } else {
    dodajToken(operandy, &liczbaOperandow, token);
  }
}

void obliczMetrykiHalsteada() {
  int L1 = 0, L2 = 0, n1 = liczbaOperatorow, n2 = liczbaOperandow;

  for (int i = 0; i < liczbaOperatorow; i++) {
    L1 += operatory[i].count;
  }

  for (int i = 0; i < liczbaOperandow; i++) {
    L2 += operandy[i].count;
  }

  int l = n1 + n2;
  int L = L1 + L2;
  double V = L * (log2(l) ? log2(l) : 1);
  double T = (n1 * L2) / (2.0 * (n2 ? n2 : 1));
  double E = V * T;
  double N[2] = { V / 3000.0, pow(E, 2.0 / 3.0) / 3000.0 };

  printf("Metryki Halsteada:\n");
  printf("Liczba różnych operatorów (n1): %d\n", n1);
  printf("Liczba różnych operandów (n2): %d\n", n2);
  printf("Łączna liczba operatorów (L1): %d\n", L1);
  printf("Łączna liczba operandów (L2): %d\n", L2);
  printf("Słownik programu (l): %d\n", l);
  printf("Długość programu (L): %d\n", L);
  printf("Objętość programu (V): %f\n", V);
  printf("Trudność programu (T): %f\n", T);
  printf("Wymagany nakład pracy (E): %f\n", E);
  printf("Przewidywana liczba błędów (N): [%f, %f]\n", N[0], N[1]);

  printf("\nOperatorzy:\n");
  for (int i = 0; i < liczbaOperatorow; i++) {
    printf("%s: %d\n", operatory[i].token, operatory[i].count);
  }

  printf("\nOperandy:\n");
  for (int i = 0; i < liczbaOperandow; i++) {
    printf("%s: %d\n", operandy[i].token, operandy[i].count);
  }
}

int main(int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Użycie: %s <plik_źródłowy>\n", argv[0]);
    return 1;
  }

  FILE *plik = fopen(argv[1], "r");
  if (!plik) {
    fprintf(stderr, "Błąd otwarcia pliku %s.\n", argv[1]);
    return 1;
  }

  char token[100];
  while (fscanf(plik, "%99s", token) != EOF) {
    przetworzToken(token);
  }

  fclose(plik);

  obliczMetrykiHalsteada();

  return 0;
}
