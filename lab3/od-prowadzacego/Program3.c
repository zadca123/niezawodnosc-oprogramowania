#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

struct Node {
  char dane;
  struct Node *next;
};
typedef struct Node *NodePtr;


int DodajElm (NodePtr *startPtr, char WstZnak) {
  struct Node *newPtr, *currPtr;
  newPtr = (struct Node *)malloc(sizeof(struct Node));
  if (newPtr == NULL)
    return 1;
  else
    {
      newPtr -> dane = WstZnak;
      newPtr -> next = NULL;

      if (*startPtr == NULL)
        *startPtr = newPtr;
      else
        {
          currPtr = *startPtr;
          while (currPtr->next != NULL)
            currPtr = currPtr -> next;
          currPtr->next = newPtr;
        }
    }
  return 0;
}


int main(void) {
  NodePtr poczListy; char Znak;
  struct Node *biezacyPtr, *elementPtr;

  poczListy=NULL;
  printf("Wprowadz znak\n");
  Znak=getche();
  while (Znak!='/') {
    DodajElm(&poczListy, Znak);
    printf("\nWprowadz kolejny znak\n");
    Znak=getche();
  }
  printf("\nKoniec tworzenia listy");

  biezacyPtr=poczListy;
  printf("\nPrzegladanie listy\n");
  while (biezacyPtr!=NULL) {
    Znak=biezacyPtr->dane;
    printf("%c\n",Znak);
    biezacyPtr=biezacyPtr->next;
  }
  printf("Koniec przegladania listy");
  getche();
  return 0;
}
